FROM mcr.microsoft.com/mssql/server:2017-latest

COPY AdventureWorks.bak /var/opt/mssql/backup/

workdir /var/opt/mssql/backup/

CMD /opt/mssql-tools/bin/sqlcmd -U 'sa' -P 'IDSI2023@ginfo' -Q "RESTORE DATABASE AdventureWorks FROM DISK='AdventureWorks.bak';" & /opt/mssql/bin/sqlservr